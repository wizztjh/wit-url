# start dev server

```Typescript
cp .env.example .env
docker compose up -d
yarn install
yarn prisma db push
yarn dev
```

# run prisma studio

```
yarn prisma studio
```

# run test

```
yarn test:all
```
