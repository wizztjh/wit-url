import { test, expect } from '@playwright/test';

test('shorten the url in a page', async ({ page}) => {
  await page.goto('/');
  await page.getByRole('textbox').click();
  await page.getByRole('textbox').fill('http://google.com');
  await page.getByRole('button', { name: 'Generate!' }).click();

  await expect(page.getByText("success! Generated url here.")).toBeVisible()
})
