import { test, expect } from '@playwright/test';

test('send a url to api for shorten', async ({ request, page}) => {
  const newShortUrl = await request.post(`/api/urls`, {
    data: {
      url: 'http://google.com',
    }
  });

  expect(newShortUrl.ok()).toBeTruthy();

  const expectedJson = {
    shortUrl: expect.stringMatching(/localhost:3003\/\w{4,8}/),
    url: "http://google.com",
  };
  const resJson = await newShortUrl.json()
  expect(resJson).toEqual(expectedJson);

  // test the short url redirection
  await page.goto(resJson.shortUrl)
  await page.waitForURL(/google/)
  expect(page.url()).toMatch(/google\.com/)
});
