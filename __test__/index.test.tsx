import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';

import Home from '@/pages/index';
import fetchMock from "jest-fetch-mock";


describe('Home', () => {
    beforeEach(() => {
        fetchMock.enableMocks();
        fetchMock.resetMocks();
    });
    it('renders a heading', () => {
        render(<Home />)

        const heading = screen.getByRole('heading', {
            name: /Wit URL/i,
        })

        expect(heading).toBeInTheDocument()
    })

    it('will do a post request to urls api to create short url', async () => {
        fetchMock.mockOnce(JSON.stringify({
            "shortUrl": "http://localhost:3000/f64e7a",
            "url": "http://google.com"
        }))


        render(<Home />)

        await userEvent.type(screen.getByRole('textbox', { name: "Shorten your url here ⚡" }), "http://google.com")
        await userEvent.click(screen.getByText('Generate!'))

        expect(fetchMock).toHaveBeenCalledTimes(1);
        expect(screen.getByText("success! Generated url here.")).toBeVisible()
        expect(screen.getByText("http://localhost:3000/f64e7a")).toBeVisible()
    })
})
