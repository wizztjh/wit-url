import { type NextApiRequest, type NextApiResponse } from 'next';
import { PrismaClient } from '@prisma/client';
import { createHash } from 'crypto';

const prisma = new PrismaClient();

type ShortUrlPostRequestData = {
    url: string
}

const generateHash = (id: number): string =>{
  const timestamp: number = Math.floor(Date.now() / 1000)
  const strToHash = `${timestamp}-${id}`
  const hash = createHash("shake256", { outputLength: 3 })
    .update(strToHash).digest("hex")
  return hash
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    const { url }: ShortUrlPostRequestData = req.body as ShortUrlPostRequestData;
    const host : string = req.headers.host || "";
    if (!url) {
      return res.status(400).json({ error: 'Please provide a URL to shorten.' });
    }

    const hash = generateHash(await prisma.shortUrl.count() + 1);

    const shortUrl = await prisma.shortUrl.create({
      data: {
        url,
        hash,
      },
    });

    return res.status(200).json({
      shortUrl: `http://${host}/${shortUrl.hash}`,
      url: shortUrl.url,
    });

  }

  return res.status(405).json({ error: 'Method Not Allowed' });
}
