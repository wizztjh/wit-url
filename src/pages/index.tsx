
import { type NextPage } from "next";
import { useForm, type SubmitHandler } from "react-hook-form";

import Head from "next/head";

import { useState } from "react";

interface IFormInput {
  url: string;
}

interface urlsJSON {
  url: string,
  shortUrl: string
}

const Home: NextPage = () => {
  const { register, handleSubmit } = useForm<IFormInput>();
  const [shortUrl, setShortUrl] = useState("");
  const [isLoading, setLoading] = useState(false)

  const onSubmit : SubmitHandler<IFormInput> = async <T extends IFormInput>(data: T ) => {
    const JSONdata = JSON.stringify(data)
    const endpoint = '/api/urls'
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSONdata,
    }
    setLoading(true);
    try {
      const res = await fetch(endpoint, options);
      const data = await res.json() as urlsJSON;
      setShortUrl(data.shortUrl);
      setLoading(false);
    } catch(e) {
      setLoading(false);
      console.error(e)
    }

  };
  interface newShortUrlToastProps {
    newLink: string
  }
  const NewShortUrlToast = (props: newShortUrlToastProps) =>{
    if (isLoading){
      return <section>loading!</section>
    } else {
      if(props.newLink){
        return (
          <section>
            <h6>
              success! Generated url here.
              <br></br>
              <a target="_blank" href={props.newLink}>{props.newLink}</a>
            </h6>
          </section>
        )
      } else {
        return <section></section>
      }
    }
  }

  return (
    <>
      <Head>
        <title>Short URL Gen</title>
        <meta name="description" content="generate short url" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <header>
        <h1>Wit URL</h1>
      </header>
      <main>
        <section>
          <form onSubmit={handleSubmit(onSubmit)}>
            <p>
              <label htmlFor="form-url">Shorten your url here ⚡</label>
              <input type="url" placeholder="http://example.com/" id='form-url' {...register("url", { required: true})} />
            </p>
            <input type="submit" disabled={isLoading} value="Generate!" />
          </form>
        </section>
        <NewShortUrlToast newLink={shortUrl}></NewShortUrlToast>
      </main>
    </>
  )
};

export default Home;
