import { PrismaClient } from '@prisma/client';
import { useRouter } from 'next/router'
import { type GetServerSideProps } from 'next';

const prisma = new PrismaClient();

export const getServerSideProps: GetServerSideProps = async ({ res, req, query }) => {
  const { hash } = query
  const host: string = req.headers.host || ""

  if (typeof hash == "string") {
    const shortUrl = await prisma.shortUrl.findUnique({
      where: { hash: hash },
    });

    if (shortUrl != null) {
      return {
        redirect: {
          permanent: false,
          destination: shortUrl.url
        },
      };
    }
  }

  return {
    redirect: {
      permanent: false,
      destination: "/",
    },
  };
};

export default function Redirect() { return }

